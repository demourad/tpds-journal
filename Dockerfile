FROM debian:10.10-slim

RUN echo " \
    deb    [check-valid-until=no, trusted=yes] http://snapshot.debian.org/archive/debian/20210707T150931Z/ buster main contrib non-free \n\
    deb-src [check-valid-until=no, trusted=yes] http://snapshot.debian.org/archive/debian/20210707T150931Z/ buster main contrib non-free \n\
    deb     [check-valid-until=no, trusted=yes] http://snapshot.debian.org/archive/debian-security/20210707T150931Z/ buster/updates main contrib non-free \n\
    deb-src [check-valid-until=no, trusted=yes] http://snapshot.debian.org/archive/debian-security/20210707T150931Z/ buster/updates main contrib non-free" > /etc/apt/sources.list 

RUN	apt-get -o Acquire::Check-Valid-Until=false update; \
    apt-get install -y texlive-base

RUN apt-get install -y texlive-latex-extra
RUN apt-get install -y texlive-science
RUN apt-get install -y texlive-bibtex-extra
RUN apt-get install -y texlive-extra-utils 
RUN apt-get install -y texlive-fonts-extra

RUN apt-get install -y \
    make \
    emacs \
    ess
RUN apt-get install -y \
    latex-make
RUN apt-get install -y \
    r-base-core \
    r-cran-ggplot2 \
    r-cran-dplyr \
    r-cran-lubridate \
    r-cran-stringr \
    r-cran-tidyr \
    r-cran-data.table \
    r-cran-gtable

RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# installing ggforce and dependencies manually
RUN R -e "install.packages('farver', version='2.1.0', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('tweenr', version='1.0.2', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('polyclip', version='1.10.0', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('RcppEigen', version='0.3.3.9.1', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('ggforce', version='0.3.3', dependencies=FALSE, repos='https://cran.rstudio.com/')"

# installing gghighlight and dependencies manually
RUN R -e "install.packages('tibble', version='3.1.2', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('tidyselect', version='1.1.1', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('vctrs', version='0.3.8', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('pillar', version='1.6.1', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('dplyr', version='1.0.7', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('ggrepel', version='0.9.1', dependencies=FALSE, repos='https://cran.rstudio.com/')"
RUN R -e "install.packages('gghighlight', version='0.3.2', dependencies=FALSE, repos='https://cran.rstudio.com/')"

WORKDIR /tpds-journal/
ENTRYPOINT [ "make"]
