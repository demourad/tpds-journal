all: journal.tex journal.pdf

rebuttal: tpds_rebuttal.tex tpds_rebuttal.pdf tpds_rebuttal_phase2.tex tpds_rebuttal_phase2.pdf

diff: journal_diff.tex journal_diff.pdf

%.tex: %.org
	emacs -l init.el -batch $^  --funcall org-babel-tangle
	emacs -l init.el -batch --eval "(require 'package)" --eval "(package-initialize)" \
		--eval "(setq enable-local-eval t)" --eval "(setq enable-local-variables t)" \
		--eval "(setq org-export-babel-evaluate t)" $^ --funcall org-latex-export-to-latex

%.pdf: %.tex
	$(PDFLATEX) $^
	bibtex `basename $^ .tex`.aux
	$(PDFLATEX) $^
	$(PDFLATEX) $^

PDFLATEX=pdflatex -shell-escape

# include LaTeX.mk
