#+TITLE: Answers to reviewers on manuscript TPDS-2020-05-0284.R1
#+AUTHOR: B. Donassolo, A. Legrand, P. Mertikopoulos, I. Fajjari
#+DATE: \today
#+STARTUP: overview indent inlineimages logdrawer
#+LANGUAGE:  en
#+TAGS: noexport(n) 
#+EXPORT_SELECT_TAGS: export  
#+EXPORT_EXCLUDE_TAGS: noexport
#+OPTIONS:   H:4 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:nil skip:nil d:nil todo:t pri:nil f:nil  tags:not-in-toc
#+LaTeX_CLASS: article
#+LaTeX_CLASS_OPTIONS: [a4paper,twoside] 
#+LATEX_HEADER: \pdfminorversion=4 % tell pdflatex to generate PDF in version 1.4
#+LATEX_HEADER: \usepackage{amsmath,amssymb}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{url} \urlstyle{sf}
#+LATEX_HEADER: \usepackage{fullpage}
#+LATEX_HEADER: \usepackage{subfigure}
#+LATEX_HEADER: \usepackage{boxedminipage}
#+LATEX_HEADER: \usepackage{palatino}
#+LATEX_HEADER: \providecommand{\hypersetup}[1]{}
#+LATEX_HEADER: \usepackage[normalem]{ulem}
#+LATEX_HEADER: \usepackage{color,colortbl}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage{caption}
#+LATEX_HEADER: \usepackage{tikzsymbols}
#+LATEX_HEADER: \usepackage{ctable}         % for booktabs / beautiful tables
#+LATEX_HEADER: \usepackage{longtable}
#+LATEX_HEADER: \usepackage{rotating,pdflscape, afterpage, pifont, ltablex}


#+BEGIN_EXPORT latex
\let\quoteo=\quote
\def\quote{\quoteo\it}
\newsavebox{\quotebox}
\renewenvironment{quote}{
   \noindent\begin{lrbox}{\quotebox}%
     \begin{boxedminipage}[t]{\linewidth}\it%
    }{
     \end{boxedminipage}%
   \end{lrbox}%
   \raisebox{1em}{\usebox{\quotebox}}\\[.2em]
}
#+END_EXPORT

* Latex configurations                                               :ignore:

** Redefine commands                                                :ignore:

#+BEGIN_EXPORT latex
\let\oldcite=\cite
\renewcommand\cite[2][]{~\ifthenelse{\equal{#1}{}}{\oldcite{#2}}{\oldcite[#1]{#2}}\xspace}
\let\oldref=\ref
\def\ref#1{~\oldref{#1}\xspace}
\def\eqref#1{~(\oldref{#1})\xspace}
\def\ie{i.e.,\xspace}
\def\eg{e.g.,\xspace}
\def\etal{~\textit{et al.\xspace}}
\let\oldpageref=\pageref
\def\pageref#1{~\oldpageref{#1}\xspace}
#+END_EXPORT

* Your Response (Text Field in ManuscriptCentral)                  :noexport:

* Introduction                                                       :ignore:


#+LaTeX: \newpage

* Answers to Reviewer 1 @@latex: \\ \large {REC: Author Should Prepare A Major Revision For A Second Review} @@
We thank the reviewer for their time and feedback. We tried to take
their recommendations into account and to clarify everything as much
as possible although adding all the suggested tables is unfortunately not an option
given the page limit for regular TPDS articles. We hope they will
nevertheless appreciate the revised version.
1) 
   #+BEGIN_QUOTE
   Abstract: ‘Finally, we show that a reactive and greedy strategy,
   which relies on this additional information…’-- how this
   information will be collected? 
   #+END_QUOTE
   The previous sentence indicates:

   #+latex: \bgroup\color{blue}
   This information mainly concerns the application's resource usage
   (estimated by the user during the design of the application) and
   the availability of resources in the infrastructure (collected by
   commercial off-the-shelf monitoring tools).
   #+latex: \egroup

2) 
   #+BEGIN_QUOTE
   Therefore, to provide a strong evidence
   of discussing recent works (to improve literature review), I still
   recommend to include more recent works as suggested below:

   - I. “A Novel Strategy to Achieve Bandwidth Cost Reduction and Load
     Balancing in a Cooperative Three-Layer Fog-Cloud Computing
     Environment,” IEEE Access, 2020. (resource scheduling, cost and
     delay trade-off)

   - II. "Dynamic on-demand fog formation offering on-the-fly IoT
     service deployment," IEEE Transactions on Network and Service
     Management (2020).
   #+end_quote
   Both articles are indeed recent and fall in the category of works
   relying on Linear/Constraint programming. However, they are purely
   centralized and ignore the dynamic reconfiguration aspect we
   address in our work, which is why we had initially decided not to
   include them. Following the suggestion of the reviewer, we have now
   included them as references 10 and 11. The text has been modified
   as follows:
   
   #+latex: \bgroup\color{blue}
   Some works apply a similar approach to deal with the
   reconfiguration in the Fog. The authors in [7]–[11] study the
   provisioning problem, i.e., where to run the applications’
   components in the Fog infrastructure. In their proposals, the
   provisioning is modeled as an ILP (Integer Linear Programming)
   problem, considering the constraints in term of resources
   (e.g. CPU, RAM) used by applications in the model.
   #+latex: \egroup

   #+BEGIN_QUOTE
   Sec-3: Related Work- Majority of the references are not recent
   (from 2016 or previous). A significant portion of works discussed
   are from 2010 or earlier. Therefore, to provide a strong evidence
   of discussing recent works (to improve literature review), I still
   recommend to include more recent works as suggested below.
   #+END_QUOTE
   We believe that this claim on references that would not be recent
   enough is not really fair. Our work evaluates techniques from the
   /learning/ and from the /scheduling/ domain in a /fog/ context. Here is a
   classification of our references by type and year:
   - Fog, IoT, cloud context :: \quad
     - 2020: [9], [10], [21] (references [9] and [10] are the two 2020
       references suggested by the reviewer above, and which we have
       added according to his recommendation)
     - 2019: [12], [20], [22], [23], [27]
     - 2018: [9], [18]
     - 2017: [7], [8], [13]
     - 2016: [2], [3], [14], [15], [27]
     - 2015: [25], [26]
     - 2013: [4]
     - 2012: [1]
   - Learning techniques :: \quad
     - 2019: [20]
     - 2018: [29]
     - 2013: [30]
     - 2009: [32]
     - 2002: [18], [19]
     - 1995: [30
   - Scheduling techniques :: \quad
     - 2009: [5]
     - 1995: [6]
   The vast majority of our references (>75%) to the fog context,
   which is a rather recent domain, were thus already from 2016 to
   2019 and are broken down in our related work by the optimization
   technique on which they rely. Regarding learning and scheduling
   techniques, which come from much older domains, we chose to refer
   to the founding articles of the domain, which is why many of these
   references dates from before 2010. We are perfectly aware or recent
   and involved developments in the learning and scheduling domains
   but many of them are not particularly helpful in our context.

   #+begin_quote
   A comparison table needs to be added within the related work
   section to discuss the novelty of this work compared to the
   existing works. Please add a comparison table (please look at
   suggested reference I to get an idea about including this
   comparison table.)
   #+END_QUOTE

   We have studied the table provided in reference I. It is similar to
   the two tables we have provided in our previous rebuttal and where
   we had provided an in-depth (8 pages) overview of existing Fog
   architectures, describing their characteristics and comparing their
   ability to manage the Fog environment, with a particular emphasis
   on provisioning and reconfiguration. As explained in our previous
   rebuttal, our article is already 17 pages long and will be subject
   to significant mandatory overlength page charges since the current
   TPDS page limit for regular TPDS articles is 12 pages, with a
   maximum of 18 pages. *Adding a comparison table of the literature is
   therefore not possible at this point.*
   
3) 
    #+BEGIN_QUOTE
    Please avoid writing large captions for the figures. Include the text in discussion and make the caption small.
    #+END_QUOTE
    We have reworked the captions according to the reviewer's
    suggestions and the average number of words by caption is now 14
    words while the larger one (Figure 7) is 39 words, which fits on
    four half-lines and indicates:
   
    #+latex: \bgroup\color{blue}
       "Workload - System Load (for an experiment). On the y-axis, we
       present the number of hosts that are close to saturation and cannot
       run more intensive applications. The dotted line marks the number
       of hosts in the system (15)."
    #+latex: \egroup
    # grep caption journal.tex | grep -v % | grep -v usepackage | sed -e 's/.*caption{//' -e 's/} *$//' | wc
    
4) 
    #+BEGIN_QUOTE
    Explain how the end devices are connected to the fog layer in this experiment.
    #+END_QUOTE
    This information is available in /Section 4.2.1 - Platform/, where we
    have modified the relevant text as follows:

    #+latex: \bgroup\color{blue}
    "From these nodes, A8 boards form the Mist layer, on which the
    software stack (IoT application and monitoring tools) runs directly
    on bare metal due to their constrained capacities. These nodes are
    generally used for control and have an Ethernet card that allows
    them to communicate with the Grid'5000 nodes through a VPN.
    Additionally, as illustrated in Fig. 4, the M3 boards (with light,
    temperature, pressure, accelerometer sensors similarly to the A8
    boards) have global IPv6 addresses and communicate with a border
    router that forwards the messages to other nodes via SLIP (Serial
    Line Internet Protocol)."
    #+latex: \egroup
   
    Also, for our experiments, the relevant text has been modified as
    follows:

    #+latex: \bgroup\color{blue}
    "In our experiments however, we restrict to A8 nodes and we
    emulate the data generated by our sensors. By doing so, we better
    control the behavior of our applications and can therefore create
    different application profiles."
    #+latex: \egroup
   
5) 
    #+BEGIN_QUOTE
    How many hosts are used?
    #+END_QUOTE
    This information is available in Section 4.1.1 and is now more specific:

    #+latex: \bgroup\color{blue}
    "In our experiments, we use /50 A8 nodes/ from FIT/IoT-LAB to
    represent the sensors, and /17 Dell PowerEdge C6100 nodes with
    Intel Xeon X5670 CPUs/ from Grid'5000 (2 of which being dedicated
    to the monitoring and experiment management), forming the Fog layer
    where applications run."
    #+latex: \egroup
   
6) 
    #+BEGIN_QUOTE
    Clarify the type of sensors used to conduct this experiment like
    temperature sensor, smoke sensor etc. Otherwise, at least provide
    their requirement. A table with parameter specification can improve
    the readability.
    #+END_QUOTE  

    This information is available in Section 4.1.1 and the relevant
   text has been augmented as follows:

    #+latex: \bgroup\color{blue}
    "From these nodes, A8 boards form the Mist layer, on which the
    software stack (IoT application and monitoring tools) runs directly
    on bare metal due to their constrained capacities. These nodes are
    generally used for control and have an ethernet card that allows
    them to communicate with the Grid'5000 nodes through a VPN.
    Additionally, as illustrated in Fig. 4, the M3 boards (with _light,
    temperature, pressure, accelerometer sensors_ _similarly to the A8
    boards_) have global IPv6 addresses and communicate with a border
    router that forwards the messages to other nodes via SLIP (Serial
    Line Internet Protocol).  _In our experiments, however, we restrict
    to A8 nodes and we emulate the data generated by our sensors._ _By
    doing so, we better control the behavior of our applications_ _and
    can therefore create different application profiles._"
    #+latex: \egroup

   Moreover, the requirement information for each component of our
   workload is available in Section 4.2.2.

7) 
   #+BEGIN_QUOTE
   A notation summary table with the notations used in the paper could make the work easier to the readers to understand.
   #+END_QUOTE  
   We thank the reviewer for his suggestion and we have added, in
   Section 6.2, a table with the common notation used by the different
   algorithms.

   #+BEGIN_EXPORT latex
   \begin{table}[h]
   \color{blue}   
   \begin{tabularx}{\linewidth}{l|X}
   \hline
   Notation & Description\\
   \hline
   $a^j_t$ & the host chosen by application $j$ at time step $t$ \\
   $\mu^{j}_{a,t}$ & the reward obtained by application $j$ at time $t$ when choosing host $a$ \\
   $\hat{\mu}^{j}_{a,t}$ & the empirical mean reward observed by application $j$ for host $a$ up to time $t$\\
   \hline
   \end{tabularx}
   \caption*{\color{blue}TABLE 1: Common notations used by the strategies}
   \end{table}
   #+END_EXPORT
 
   There are actually very few mathematical notations in this article:
   one paragraph in the beginning of Section 5 (page 7), mostly to
   define $a^j_t$, the action chosen by application $j$ at time $t$ and
   the overall cost minimization objective), and less than two pages
   in sections 6.2.1-6.2.5 (page 9-11) to properly define all the
   bandit variants we investigate. The reading difficulty may arise
   from the fact that we present 6 learning strategies with different
   degrees of sophistication. We kept our notations to a bare minimum
   and chose them carefully so that they are aligned with the ones
   commonly used in the papers from the literature on which we
   build. All other notations (e.g., $\tau, \alpha, \eta$) are thus specific to a
   given bandit algorithm, which is why we kept the notation table
   short with only the common notations.
 
8) 
   #+BEGIN_QUOTE
   Why Satisfaction threshold is set as 2s? In the revised version just mentioned ‘arbitrarily’. Please give reasons for choosing 2s from practical scenario.
   #+END_QUOTE  

   Thanks for bringing this up. We have now justified this
   Satisfaction threshold as follows in section 6.2.1:

   #+latex: \bgroup\color{blue}
   In our setup, the threshold is set to /2 seconds/ in average over the
   last 20 messages, which is five times larger than the typical delay
   when the platform is dedicated to the application.
   #+latex: \egroup
   
9) 
   #+BEGIN_QUOTE
   Previous comment: “Instead of making the conclusion too large, a
   summary of the key observation section should be added.” is not
   addressed, rather, previous conclusion is subdivided into
   conclusion and future work which is even larger than before.
   #+END_QUOTE  
   We understand the reviewer's concern, and we have followed the
   reviewer 2's suggestion to divide the "Conclusion" section in three
   subsections
   #+latex: \bgroup\color{blue}
   (Summary of the Work, Discussion and Guidelines, and Future Work).
   #+latex: \egroup
   The result is not
   really shorter but now conveys a much clearer message to the
   reader. We hope the reviewer will like this version better.
#+LaTeX: \newpage

* Answers to Reviewer 2 @@latex: \\ \large {REC: Author Should Prepare A Minor Revision (no need for additional review if the reviewer address the suggested modifications)} @@

#+BEGIN_QUOTE
Comments:
This is review for the revised version submitted by the authors.  The authors
have addressed my previous concerns. They now concretely specify what they mean
by reconfiguration. Authors have also provided links to the open source
artifacts. This will immensely help the scientific community who may want to
adopt the code base and experimental setup.  Overall, I think the paper has
improved quite a bit and illustrates a contribution that is strong in the
application of the theory, the systematic evaluations conducted, and insights
derived.

Below are some cosmetic suggested changes. Authors can easily fix these and do
not need additional review.
#+END_QUOTE

We would like to thank the reviewer for his/her insightful and
comprehensive comments. We have integrated most of them and we believe
that the paper is considerably better now. We provide below a
point-to-point reply to the reviewer's suggestions and comments:

1) 
   #+BEGIN_QUOTE
   Abstract: suggestion to replace the phrase "decelerate its
   adoption" to "hinders its adoption"
   #+END_QUOTE
   Thanks for the suggestion, we have modified the text as suggested.

   #+BEGIN_EXPORT latex
   { \color{blue}
   "In this context, Fog computing emerges as a potential solution, providing nearby resources to run IoT applications.
   However, the Fog raises several challenges which hinders its adoption."
   }
   #+END_EXPORT

2)
   #+BEGIN_QUOTE
   Intro section, page 1, col 1: at the bottom of the page the sentence reads
   "adapt the resources used by applications to keep them satisfied
   ...". While this sentence is fine, it makes sense for people but
   sounds strange for applications. Is there another way to say this?
   For instance, "adapt the resources to satisfy the quality of
   service needs of applications and maintain their smooth execution"?
   #+END_QUOTE
   Thanks for the feedback. We have reworded the phrase to make it
   clearer:

   #+BEGIN_EXPORT latex
   { \color{blue}
   "The reconfiguration aims to adapt the resources used by
   applications to satisfy the quality of service and maintain their
   smooth execution."
   }
   #+END_EXPORT

3)
   #+BEGIN_QUOTE
   Intro section last para: Change "high quantity and complexity of
   to "large number and complexity of". I am also wondering if it
   makes sense to put this para just before section 6.1. Typically the
   intro section ends after the organization of the paper is
   described.
   #+END_QUOTE
   We have modified the text as suggested. Additionally, we have
   reworked the beginning of Section 6 to add this paragraph.

   #+BEGIN_EXPORT latex
   { \color{blue}
   "Throughout this section, we will describe several reconfiguration
   strategies, ranging from simple baseline strategies to advanced online
   learning and greedy strategies. We opt for presenting them
   incrementally, explaining the motivations for implementing each
   strategy. Due to the large number and complexity of the
   reconfiguration strategies presented during this paper, we would like
   to point out to the readers the sections which summarize our main
   results : i) Table 1 summarizes and compares the
   key features of the reconfiguration strategies; ii)
   Fig. 12 and Fig. 15 present
   the results of these strategies in the scenario with reliable
   application information; and iii) Fig. 17 shows
   the performance of the strategies in the inaccurate information
   scenario."
   }
   #+END_EXPORT

4)
   #+BEGIN_QUOTE   
   Heading of section 2.2 as "The Reconfiguration" seems a bit strange. Do you
   want to say "The Reconfiguration Problem"?. In that subsection, the
   Fig is good. But I am wondering if someone may get lost not knowing
   what "ramp up" means? Can you add a sentence explaining what it
   means. Basically, it is the initial deployment. Then
   reconfiguration happens as resources fluctuate.
   #+END_QUOTE
   Thanks for the suggestion, we agree that "The Reconfiguration
   Problem" is clearer to readers. In addition, we have improved the
   text to explain the ramp up phase. The new sentence reads as
   follows.

   #+BEGIN_EXPORT latex
   { \color{blue}
   The experiment includes two phases: ramp-up and online
   reconfiguration. The ramp-up is the initial deployment stage, where we
   assume that all applications arrive and are placed by the provisioning
   algorithm. During the online reconfiguration stage, the performances
   of the applications are optimized considering the resource usage
   evolution.
   }
   #+END_EXPORT

5)
   #+BEGIN_QUOTE
   Section 4.2.2, below fig 5, change "are inspired in video ..." to "are
   inspired by video ...". In the same sentence, insert an "a" before
   "calm application represents ..."
   #+END_QUOTE
   Thanks, the text has been modified as suggested.

   #+BEGIN_EXPORT latex
   { \color{blue}
   The intensive applications are inspired by video streaming
   applications, whose video coding process accounts for 90\% of the total
   delay[25]. On the other hand, a calm application
   represents the processing of a simple information (e.g. reading a
   temperature sensor).
   }
   #+END_EXPORT

6)
   #+BEGIN_QUOTE
   In the same section, can you give a rationale/justification for selecting a
   50-50 mix of intensive and calm applications under the bullet for Application
   heterogeneity.
   #+END_QUOTE
   The main rationale for choosing a 50%/50% mix of applications is
   to favor heterogeneity. In doing so, we avoid strategies that
   would benefit only certain types of applications at the expense of
   others. The text in this item has been modified as follows.

   #+BEGIN_EXPORT latex
   { \color{blue}
   - \textbf{Application heterogeneity}: is the mix of applications present in the
   workload. In our experiments, we target a workload as heterogeneous as possible as
   we aim at ensuring that all kind of applications equally benefit from the reconfiguration 
   strategies. Thus, we opt for a 50\%/50\% mix between intensive
   and calm applications.
   }
   #+END_EXPORT

7)
   #+BEGIN_QUOTE
   In the Satisfaction threshold bullet of 4.2.2, suggestion to change
   "applications are not satisfied with ..." to "applications do not
   meet their delay requirements with the current ..."
   #+END_QUOTE
   Thanks for the proposal, we have modified the text as follows:

   #+BEGIN_EXPORT latex
   { \color{blue}
   - \textbf{Satisfaction threshold}: the acceptable end-to-end delay for
   applications. Above this threshold, the applications do not meet
   their delay requirements with the current placement and should be
   migrated. In our setup, the threshold is set to \textit{2 seconds} in average
  over the last 20 messages, which is five times larger than typical
  delay when the platform is dedicated to the application.
   }
   #+END_EXPORT

8)
   #+BEGIN_QUOTE
   Section 5 heading: Some reader may wonder why this is being referred to as
   "Game Overview". I agree that the related work mentioned a variety
   of related works along this line but none of the Intro section
   contribution bullets really say anything about Game Theory.  Do you
   want to rename this heading to "Game Theoretic Formulation of the
   Reconfiguration Problem"? I think this will make sense because
   section 2.2 is called "The Reconfiguration Problem" (see my
   suggestion above).
   #+END_QUOTE
   Following the feedback in items 4) and 8), we have modified both
   titles according to your propositions.

   #+BEGIN_EXPORT latex
   { \color{blue}
   5 Game-Theoretic Formulation of the Reconfiguration Problem
   }
   #+END_EXPORT

9)
   #+BEGIN_QUOTE
   Section 6.1.1: I am wondering if the term "Lazy" will cause confusion to
   readers. Lazy implies that something will be done with a delay,
   i.e., a reconfig will happen but it will happen is a lazy
   manner. But in your case, it is effectively no reconfiguration at
   all. Why not just call it something like "No Reconfig" or "Do
   Nothing" - maybe the latter is better?
   #+END_QUOTE
   Thanks for your feedback. We use the term "lazy" to indicate
   something that is unwilling to work or use energy to change. We
   would like to name the strategies using the simplest possible
   one-word name and we have not been able to find a better one.  We
   use a second name for each strategy to clarify its meaning to the
   readers, such as "Anarchy: total freedom" or "Totalitarian:
   clairvoyant dictatorship". We hope that the name chosen for Section
   6.1.1
   #+latex: \bgroup\color{blue}
   "Lazy: no reconfiguration"
   #+latex: \egroup
   will clear up any remaining doubts.

10)
   #+BEGIN_QUOTE
   Section 7: I am somewhat confused why the term "Affinity" is used to indicate
   a scenario when incorrect or inadequate resource information is available.
   Affinity means attraction to something. For example, when scheduling threads
   to specific cores, we set thread affinity to a specific core so it almost
   always get scheduled there. I am not sure how that sort of meaning is valid
   in this context?
   #+END_QUOTE
   Yes, this is exactly the meaning we would like to transmit in this
    case. In the "Affinity Scenario", applications have an attraction for
    some hosts, on which their performance is greatly
    improved. Consequently, they would have better performance if
    scheduled on these hosts.  However, the information of which host
    is best for each application is unknown and therefore, the
    requirements described by the user is incorrect. For this reason,
    we name it as a scenario with "incorrect" or "inadequate" resource
    information.

   We have reworked the beginning of Section 7 to clarify this point.

   #+BEGIN_EXPORT latex
   { \color{blue}
   In Section 6, we studied the case where users were able to
   accurately describe the resources needed by their applications. In
   this section, on the other hand, we present an application-host
   affinity scenario. In this setup, each application has hosts on which
   its performance is greatly enhanced. Ideally, applications should run
   on these special nodes. However this information is unknown when
   describing the resource utilization. We will see how this inaccurate
   information can affect the strategy's performance and compare this
   affinity scenario with the previous non-affinity one.
   }
   #+END_EXPORT

11)
   #+BEGIN_QUOTE
   Section 8: I am wondering if authors can add some guidance as to how
   developers can learn from your work and apply these principles?
   What should they look for? How should they make decisions?  Maybe
   Section 9 can be merged in Section 8 by dividing Section 8 into
   subsections (a) summary of work done, (b) Discussion and
   guidelines, and (c) open problems/future work.
   #+END_QUOTE
   We would like to thank the reviewer for pointing this out. We have
    reworked Section 8 and divided it in 3 parts as suggested. In the
    Section "Discussion and Guidelines", we have added the key
    take-away messages about our experiments and results.

   #+BEGIN_EXPORT latex
   { \color{blue}
   8.2 Discussion and Guidelines

None of the classical online learning strategies was able to obtain
satisfying performance, in particular because in our context, the
numerous migrations required by the exploration are prohibitive. Our
in-depth analysis of these strategies allowed us to mitigate this
problem by designing a mildly informed and reactive learning
strategy. 

In a complex and heterogeneous platform such as the Fog, one cannot
neglect the hidden costs incurred by the environment. Our experiments
point out that the cost of migration is unbearable and must be
mitigated when designing new reconfiguration strategies. In this
context, using any available information on resource usage, even under
uncertainty, can greatly improve performance.
   }
   #+END_EXPORT

#+LaTeX: \newpage

* References                                                         :noexport:

#+LATEX: \bibliographystyle{IEEEtran}
#+LATEX: \bibliography{refs_rebuttal}

* Bib file is here                                                 :noexport:

Tangle this file with C-c C-v t

#+begin_src bib :tangle refs_rebuttal.bib

#+end_src


* Emacs setup                                                      :noexport:
# Local Variables:
# eval: (add-to-list 'load-path ".")
# eval: (require 'ox-extra)
# eval: (ox-extras-activate '(ignore-headlines))
# eval: (add-to-list 'org-latex-classes '("article" "\\documentclass{article}\n \[NO-DEFAULT-PACKAGES]\n \[EXTRA]\n"  ("\\section{%s}" . "\\section*{%s}") ("\\subsection{%s}" . "\\subsection*{%s}")                       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")                       ("\\paragraph{%s}" . "\\paragraph*{%s}")                       ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
# eval: (setq org-latex-to-pdf-process '("pdflatex -interaction nonstopmode -output-directory %o %f ; bibtex `basename %f | sed 's/\.tex//'` ; pdflatex -interaction nonstopmode -output-directory  %o %f ; pdflatex -interaction nonstopmode -output-directory %o %f"))
# eval: (setq ispell-local-dictionary "american")
# eval: (eval (flyspell-mode t))
# End:


 
