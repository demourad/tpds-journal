# tpds-journal

This repository contains all files used to create the journal paper submitted to TPDS.

The paper and all is written with support of org-mode/emacs. The file journal.org is self-contained 

## Generating the paper
The repository contains a Makefile to create the paper in pdf format. It is supposed to run in a linux system with the correct dependencies installed (make, emacs, org-mode, pdflatex, etc).

A dockerfile is provided to generate the proper environment.

```
docker build -t tpds-latex .
docker run -v $PWD:/tpds-journal -u $(id -u):$(id -g) -it --entrypoint /bin/bash tpds-latex:latest
```

After, you just need to run the make command inside the container.
```
make
```

A journal.pdf file is created in the root directory.

## Data analysis and figures

The data analysis and all figures used in this paper are generated through R scripts, in the section "Figures" of the journal.org file.

Due to the volume of data (about 1.5Gb), the raw data is stored in a different repository:

```
git clone https://gitlab.inria.fr/demourad/data.git data
```

Please clone the repository in a subfolder named data/.


Alternatively, the data is available at Zenodo: https://doi.org/10.5281/zenodo.5078473

Finally, open the journal.org file and go to Section Figures.

```
docker run -v $PWD:/tpds-journal -u $(id -u):$(id -g) -it --entrypoint /bin/bash tpds-latex:latest
emacs -l init.el journal.org
```

## Source code

### Calvin

The source code containing the implementation of all reconfiguration methods presented in the paper is available at https://github.com/brunodonassolo/calvin-base, branch calvinDevelop122018.


```
git clone https://github.com/brunodonassolo/calvin-base.git;
git checkout calvinDevelop122018
```

This project is a fork of original Calvin project: https://github.com/EricssonResearch/calvin-base.

Moreover, it is archived in the Software Heritage initiative under the SWHID: swh:1:dir:c95e83ff7d06b9369024d64add6c17d546563a5d;
Available at: https://archive.softwareheritage.org/swh:1:dir:c95e83ff7d06b9369024d64add6c17d546563a5d;origin=https://github.com/brunodonassolo/calvin-base;visit=swh:1:snp:7e0bc02616968f93247686a9c7c6a6e9c5ceb367;anchor=swh:1:rev:c1e03e4d84d4deef88439ca483759cbac08619dd

### FITOR

The FITOR orchestrator used to perform the experiments on the testbeds is also freely available at: https://gitlab.inria.fr/demourad/fitor.

However, to re-run the experiments, you would need access to the Grid'5000 and FIT/IoT-LAB platforms.

Also, it is archived in the Software Heritage under the SWHID: swh:1:dir:e24b9e78bc3101b20d6919628f9c101cc9b33431;
Available at: https://archive.softwareheritage.org/swh:1:dir:e24b9e78bc3101b20d6919628f9c101cc9b33431;origin=https://gitlab.inria.fr/demourad/fitor.git;visit=swh:1:snp:9d7306722798882e70cac3b1dbd28522eef1eab9;anchor=swh:1:rev:ae9f3b007bd2c735ba4680fbb70b3e5373423279
